package com.google.zxing.configs

object Configs {
    @JvmField var PRINT_LOG = false
    @JvmField val TAG = "zxing"

    @JvmField val KEY_FRONT_LIGHT_MODE = "preferences_front_light_mode"
    @JvmField val KEY_AUTO_FOCUS = "preferences_auto_focus"
    @JvmField val KEY_INVERT_SCAN = "preferences_invert_scan"

    @JvmField val KEY_DISABLE_CONTINUOUS_FOCUS = "preferences_disable_continuous_focus"
    @JvmField val KEY_DISABLE_EXPOSURE = "preferences_disable_exposure"
    @JvmField val KEY_DISABLE_METERING = "preferences_disable_metering"
    @JvmField val KEY_DISABLE_BARCODE_SCENE_MODE = "preferences_disable_barcode_scene_mode"
}