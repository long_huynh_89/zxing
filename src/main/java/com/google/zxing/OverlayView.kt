package com.google.zxing

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.graphics.Region
import android.os.Build
import androidx.annotation.AnyThread
import android.util.AttributeSet
import android.view.View

class OverlayView : View {

    private var color = Color.BLACK
    private var rect: Rect? = null

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?,
                defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int,
                defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    @AnyThread
    fun setColor(color: Int) {
        this.color = color
        postInvalidate()
    }

    @AnyThread
    fun setFrameRect(rect: Rect) {
        if (this.rect != rect) {
            this.rect = rect
            postInvalidate()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        rect?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                canvas?.clipOutRect(it)
            } else {
                canvas?.clipRect(it, Region.Op.DIFFERENCE)
            }

            canvas?.drawColor(color)
        }
    }
}