package com.google.zxing

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.Rect
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup

class DebugImageView : View {

    private var bitmap: Bitmap? = null
    private val bitmapRect = Rect()
    private val canvasRect = Rect()

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?,
                defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int,
                defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun onDetachedFromWindow() {
        if (bitmap?.isRecycled == false) {
            bitmap?.recycle()
        }

        super.onDetachedFromWindow()
    }

    override fun onDraw(canvas: Canvas?) {
        val bm = bitmap
        if (bm?.isRecycled == false && canvas != null) {
            bitmapRect.set(0, 0, bm.width, bm.height)

            if (bm.width > bm.height) {
                val size = (canvas.width * bm.height) / bm.width
                canvasRect.set(0, 0, canvas.width, size)
            } else {
                val size = (canvas.height * bm.width) / bm.height
                canvasRect.set(0, 0, size, canvas.height)
            }

            canvas.drawBitmap(bm, bitmapRect, canvasRect, null)
        }
    }

    fun updateData(source: PlanarYUVLuminanceSource,
                   rect: Rect, rotate: Float) {
        if (bitmap?.isRecycled == false) {
            bitmap?.recycle()
        }

        val params = layoutParams as ViewGroup.MarginLayoutParams
        val max = Math.max(rect.width(), rect.height())

        if (params.leftMargin != rect.left
                || params.topMargin != rect.bottom + 30
                || params.width != max
                || params.height != max) {
            params.leftMargin = rect.left
            params.topMargin = rect.bottom + 30
            params.width = max
            params.height = max

            post {
                layoutParams = params
            }
        }

        if (this.rotation != rotate) {
            post { this.rotation = rotate }
        }

        bitmap = Bitmap.createBitmap(
                source.renderThumbnail(), source.thumbnailWidth,
                source.thumbnailHeight, Bitmap.Config.ARGB_8888)

        if (bitmap != null) {
            postInvalidate()
        }
    }
}