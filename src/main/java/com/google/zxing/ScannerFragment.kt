package com.google.zxing

import android.graphics.*
import android.opengl.GLES10
import android.os.*
import androidx.annotation.WorkerThread
import androidx.fragment.app.Fragment
import android.util.Log
import android.view.*
import android.widget.ImageView
import com.google.zxing.camera.CameraManager
import com.google.zxing.camera.FrameHandler
import com.google.zxing.common.HybridBinarizer
import com.google.zxing.configs.Configs
import com.google.zxing.configs.Configs.PRINT_LOG
import com.google.zxing.configs.Configs.TAG
import kotlinx.android.synthetic.main.fragment_scanner.*
import java.io.ByteArrayOutputStream
import java.lang.ref.WeakReference

class ScannerFragment : Fragment(), FrameHandler.Callback, SurfaceHolder.Callback2 {

    private val listener: OnFragmentInteractionListener? get() {
        return parentFragment as? OnFragmentInteractionListener
                ?: activity as? OnFragmentInteractionListener
                ?: context as? OnFragmentInteractionListener
    }
    private val cameraManager by lazy {
        CameraManager(context?.applicationContext)
    }
    private val workerHandler by lazy {
        FrameHandler(AsyncTask.SERIAL_EXECUTOR, this)
    }
    private val formats by lazy {
        arrayListOf(BarcodeFormat.QR_CODE)
    }
    private val multiFormatReader by lazy {
        MultiFormatReader().apply {
            setHints(mapOf(
                    Pair(DecodeHintType.POSSIBLE_FORMATS, formats)))
        }
    }
    private val handler by lazy {
        Handler(Looper.getMainLooper())
    }
    private var surfaceHolderReference: WeakReference<Surface>? = null
    private val overlayColor by lazy {
        arguments?.getInt(COLOR)
    }
    private var debugImage: DebugImageView? = null

    private var framingRect: Rect?
        get() {
            return arguments?.getParcelable(FRAMING_RECT) as? Rect
        }
        set(value) {
            arguments?.putParcelable(FRAMING_RECT, value)
        }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? = inflater.
            inflate(R.layout.fragment_scanner, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        overlayColor?.let {
            overlayView.setColor(it)
        }

        if (Configs.PRINT_LOG) {
            val imageView = DebugImageView(context)
            container.addView(imageView)

            debugImage = imageView
        }

        surfaceView.holder.addCallback(this)
    }

    private fun openDriver(holder: SurfaceHolder) {
        if (holder.surface != surfaceHolderReference?.get()) {
            cameraManager.openDriver(holder)
            surfaceHolderReference = WeakReference(holder.surface)
        }
    }

    private fun requestFrameToDecode() {
        cameraManager.requestPreviewFrame(workerHandler, R.id.decode)
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        openDriver(holder)

        val ratio = cameraManager.getRatio(width, height)
        if (ratio != null) {
            val size = cameraManager.viewSize

            if (size.width() != width
                    || size.height() != height) {
                val params = surfaceView.layoutParams
                        as ViewGroup.MarginLayoutParams
                params.leftMargin = size.left
                params.topMargin = size.top
                params.width = size.width()
                params.height = size.height()
                surfaceView.layoutParams = params
            }
            else {
                framingRect?.let {
                    cameraManager.setManualFramingRect(it)
                }

                cameraManager.startPreview()
                requestFrameToDecode()
            }
        }
    }

    @WorkerThread
    override fun handleMessage(msg: Message?): Boolean {
        return when (msg?.what) {
            R.id.decode -> {
                decode(msg.obj as ByteArray, msg.arg1, msg.arg2)
                requestFrameToDecode()
                true
            }

            else -> false
        }
    }

    @WorkerThread
    private fun decode(data: ByteArray, width: Int, height: Int) {
        val rawResult = cameraManager.buildLuminanceSource(
                data, width, height)?.let { source ->
            val rect = cameraManager.framingRect
            overlayView.setFrameRect(rect)

            debugImage?.updateData(source, rect,
                    cameraManager.cwNeededRotation.toFloat())

            val bitmap = BinaryBitmap(HybridBinarizer(source))
            try {
                multiFormatReader.decodeWithState(bitmap)
            } catch (e: ReaderException) {
                null
            } finally {
                multiFormatReader.reset()
            }
        }

        if (rawResult != null) {
            if (PRINT_LOG) {
                Log.i(TAG, "[ScannerFragment] [decode] result = $rawResult")
            }

            handler.post {
                listener?.onScanResult(rawResult.text,
                        rawResult.barcodeFormat)
            }

            requestFrameToDecode()
        }
    }

    fun setFocusRect(left: Int, top: Int, right: Int, bottom: Int) {
        val rect = Rect(left, top, right, bottom)
        cameraManager.setManualFramingRect(rect)
        framingRect = rect
    }

    override fun surfaceRedrawNeeded(holder: SurfaceHolder) {}

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        surfaceHolderReference = null

        cameraManager.stopPreview()
        cameraManager.closeDriver()
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        openDriver(holder)
    }

    interface OnFragmentInteractionListener {
        fun onScanResult(text: String, format: BarcodeFormat)
    }

    companion object {
        private const val COLOR = "COLOR"
        private const val FRAMING_RECT = "FRAMING_RECT"

        fun newInstance(overlayColor: Int): ScannerFragment {
            val bundle = Bundle()

            if (Configs.PRINT_LOG) {
                bundle.putInt(COLOR, Color.BLACK)
            } else {
                bundle.putInt(COLOR, overlayColor)
            }

            val fm = ScannerFragment()
            fm.arguments = bundle

            return fm
        }
    }
}