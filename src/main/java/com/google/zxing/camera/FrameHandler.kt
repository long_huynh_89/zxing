package com.google.zxing.camera

import android.os.Message
import java.lang.ref.WeakReference
import java.util.concurrent.Executor

class FrameHandler(private val executor: Executor, callback: Callback) {

    private val callbackReference = WeakReference(callback)
    private var isReleased = false

    fun sendMessage(what: Int, arg1: Int, arg2: Int, obj: Any) {
        if (isReleased) return

        val message = Message()
        message.what = what
        message.arg1 = arg1
        message.arg2 = arg2
        message.obj = obj

        executor.execute {
            val callback = callbackReference.get()

            if (callback != null) {
                callback.handleMessage(message)
            } else {
                isReleased = true
            }
        }
    }

    interface Callback {
        fun handleMessage(msg: Message?): Boolean
    }
}