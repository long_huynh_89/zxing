package com.google.zxing.camera

import android.hardware.Camera
import java.lang.ref.WeakReference

@Suppress("deprecation")  // camera APIs
class CameraErrorCallback (manager: CameraManager): Camera.ErrorCallback {

    private val managerReference = WeakReference(manager)

    override fun onError(error: Int, camera: Camera?) {
        managerReference.get()?.onError(error, camera)
    }
}