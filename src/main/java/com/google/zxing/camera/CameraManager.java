/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.zxing.camera;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.SurfaceHolder;

import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.camera.open.OpenCamera;
import com.google.zxing.camera.open.OpenCameraInterface;
import com.google.zxing.configs.Configs;

import java.io.IOException;

import static com.google.zxing.configs.Configs.PRINT_LOG;

/**
 * This object wraps the Camera service object and expects to be the only one talking to it. The
 * implementation encapsulates the steps needed to take preview-sized images, which are used for
 * both preview and decoding.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 */
@SuppressWarnings("deprecation") // camera APIs
public final class CameraManager {

  private static final String TAG = CameraManager.class.getSimpleName();

  private static final int CAMERA_MSG_ERROR            = 0x001;
  private static final int CAMERA_MSG_SHUTTER          = 0x002;
  private static final int CAMERA_MSG_FOCUS            = 0x004;
  private static final int CAMERA_MSG_ZOOM             = 0x008;
  private static final int CAMERA_MSG_PREVIEW_FRAME    = 0x010;
  private static final int CAMERA_MSG_VIDEO_FRAME      = 0x020;
  private static final int CAMERA_MSG_POSTVIEW_FRAME   = 0x040;
  private static final int CAMERA_MSG_RAW_IMAGE        = 0x080;
  private static final int CAMERA_MSG_COMPRESSED_IMAGE = 0x100;
  private static final int CAMERA_MSG_RAW_IMAGE_NOTIFY = 0x200;
  private static final int CAMERA_MSG_PREVIEW_METADATA = 0x400;
  private static final int CAMERA_MSG_FOCUS_MOVE       = 0x800;

  private static final int MIN_FRAME_WIDTH = 240;
  private static final int MIN_FRAME_HEIGHT = 240;
  private static final int MAX_FRAME_WIDTH = 1200; // = 5/8 * 1920
  private static final int MAX_FRAME_HEIGHT = 675; // = 5/8 * 1080

  private final Context context;
  private final CameraConfigurationManager configManager;
  private OpenCamera camera;
  private AutoFocusManager autoFocusManager;
  private Rect framingRect;
  private Rect framingRectInPreview;
  private boolean initialized;
  private boolean previewing;
  private int requestedCameraId = OpenCameraInterface.NO_REQUESTED_CAMERA;
  private int requestedFramingRectWidth;
  private int requestedFramingRectHeight;
  /**
   * Preview frames are delivered here, which we pass on to the registered handler. Make sure to
   * clear the handler so it will only receive one message.
   */
  private final PreviewCallback previewCallback;

  public CameraManager(Context context) {
    this.context = context;
    this.configManager = new CameraConfigurationManager(context);
    previewCallback = new PreviewCallback(configManager);
  }

  public void onError(int error, @Nullable Camera c) {
    switch (error) {
      case CAMERA_MSG_SHUTTER:
        stopPreview();
        closeDriver();
        break;
    }
  }

  private int ratioWidth = -1;
  private int ratioHeight = -1;
  private Rect viewSize;
  private Float ration;

  @SuppressWarnings("SuspiciousNameCombination")
  @Nullable
  public Float getRatio(int width, int height) {
      if (ratioWidth == width || ratioHeight != height || ration == null) {
          Point size = configManager.getBestPreviewSize();

          if (size != null) {
              ratioWidth = width;
              ratioHeight = height;

              int rotate = configManager.getCWNeededRotation();

              if (rotate == 90 || rotate == 270) {
                  size = new Point(size.y, size.x);
              }

              float r = size.x / (float) size.y;
              float rh = ratioWidth / r;
              float rw = ratioHeight * r;

              int ww = (int) rw;
              int hh = ratioHeight;

              if (rh > ratioHeight) {
                ww = ratioWidth;
                hh = (int) rh;
              }

              ration = size.x / (float) ww;

              int x = (ratioWidth - ww) / 2;
              int y = (ratioHeight - hh) / 2;

              viewSize = new Rect(x, y, x + ww, y + hh);
          }
      }

      return ration;
  }

  @NonNull
  public Rect getViewSize() {
      return viewSize;
  }

  public int getCWNeededRotation() {
    return configManager.getCWNeededRotation();
  }
  
  /**
   * Opens the camera driver and initializes the hardware parameters.
   *
   * @param holder The surface object which the camera will draw preview frames into.
   * @throws IOException Indicates the camera driver failed to open.
   */
  public synchronized void openDriver(SurfaceHolder holder) throws IOException {
    OpenCamera theCamera = camera;
    if (theCamera == null) {
      theCamera = OpenCameraInterface.open(requestedCameraId);
      if (theCamera == null) {
        throw new IOException("Camera.open() failed to return object from driver");
      }
      camera = theCamera;
      camera.getCamera().setErrorCallback(
              new CameraErrorCallback(this));
    }

    if (!initialized) {
      initialized = true;
      configManager.initFromCameraParameters(theCamera);
      if (tempRect != null) {
        setManualFramingRect(tempRect);
        tempRect = null;
        requestedFramingRectWidth = 0;
        requestedFramingRectHeight = 0;
      }
      else if (requestedFramingRectWidth > 0 && requestedFramingRectHeight > 0) {
        setManualFramingRect(requestedFramingRectWidth, requestedFramingRectHeight);
        requestedFramingRectWidth = 0;
        requestedFramingRectHeight = 0;
      }
    }

    Camera cameraObject = theCamera.getCamera();
    Camera.Parameters parameters = cameraObject.getParameters();
    String parametersFlattened = parameters == null ? null : parameters.flatten(); // Save these, temporarily
    try {
      configManager.setDesiredCameraParameters(theCamera, false);
    } catch (RuntimeException re) {
      // Driver failed
      if (PRINT_LOG) {
        Log.w(TAG, "Camera rejected parameters. Setting only minimal safe-mode parameters");
        Log.i(TAG, "Resetting to saved camera params: " + parametersFlattened);
      }
      // Reset:
      if (parametersFlattened != null) {
        parameters = cameraObject.getParameters();
        parameters.unflatten(parametersFlattened);
        try {
          cameraObject.setParameters(parameters);
          configManager.setDesiredCameraParameters(theCamera, true);
        } catch (RuntimeException re2) {
          // Well, darn. Give up
          if (PRINT_LOG) {
            Log.w(TAG, "Camera rejected even safe-mode parameters! No configuration");
          }
        }
      }
    }
    cameraObject.setPreviewDisplay(holder);

  }

  public synchronized boolean isOpen() {
    return camera != null;
  }

  /**
   * Closes the camera driver if still in use.
   */
  public synchronized void closeDriver() {
    if (camera != null) {
      camera.getCamera().release();
      camera = null;
      // Make sure to clear these each time we close the camera, so that any scanning rect
      // requested by intent is forgotten.
      framingRect = null;
      framingRectInPreview = null;
    }
  }

  /**
   * Asks the camera hardware to begin drawing preview frames to the screen.
   */
  public synchronized void startPreview() {
    OpenCamera theCamera = camera;
    if (theCamera != null && !previewing) {
      theCamera.getCamera().startPreview();
      previewing = true;
      autoFocusManager = new AutoFocusManager(context, theCamera.getCamera());
    }
  }

  /**
   * Tells the camera to stop drawing preview frames.
   */
  public synchronized void stopPreview() {
    if (autoFocusManager != null) {
      autoFocusManager.stop();
      autoFocusManager = null;
    }
    if (camera != null && previewing) {
      camera.getCamera().stopPreview();
      previewCallback.setHandler(null, 0);
      previewing = false;
    }
  }

  /**
   * Convenience method for {link com.google.zxing.client.android.CaptureActivity}
   *
   * @param newSetting if {@code true}, light should be turned on if currently off. And vice versa.
   */
  public synchronized void setTorch(boolean newSetting) {
    OpenCamera theCamera = camera;
    if (theCamera != null && newSetting != configManager.getTorchState(theCamera.getCamera())) {
      boolean wasAutoFocusManager = autoFocusManager != null;
      if (wasAutoFocusManager) {
        autoFocusManager.stop();
        autoFocusManager = null;
      }
      configManager.setTorch(theCamera.getCamera(), newSetting);
      if (wasAutoFocusManager) {
        autoFocusManager = new AutoFocusManager(context, theCamera.getCamera());
        autoFocusManager.start();
      }
    }
  }

  /**
   * A single preview frame will be returned to the handler supplied. The data will arrive as byte[]
   * in the message.obj field, with width and height encoded as message.arg1 and message.arg2,
   * respectively.
   *
   * @param handler The handler to send the message to.
   * @param message The what field of the message to be sent.
   */
  public synchronized void requestPreviewFrame(FrameHandler handler, int message) {
    OpenCamera theCamera = camera;
    if (theCamera != null && previewing) {
      previewCallback.setHandler(handler, message);
      theCamera.getCamera().setOneShotPreviewCallback(previewCallback);
    }
  }

  /**
   * Calculates the framing rect which the UI should draw to show the user where to place the
   * barcode. This target helps with alignment as well as forces the user to hold the device
   * far enough away to ensure the image will be in focus.
   *
   * @return The rectangle to draw on screen in window coordinates.
   */
  public synchronized Rect getFramingRect() {
    if (framingRect == null) {
      if (camera == null) {
        return null;
      }
      Point screenResolution = configManager.getScreenResolution();
      if (screenResolution == null) {
        // Called early, before init even finished
        return null;
      }

      int width = findDesiredDimensionInRange(screenResolution.x, MIN_FRAME_WIDTH, MAX_FRAME_WIDTH);
      int height = findDesiredDimensionInRange(screenResolution.y, MIN_FRAME_HEIGHT, MAX_FRAME_HEIGHT);

      int leftOffset = (screenResolution.x - width) / 2;
      int topOffset = (screenResolution.y - height) / 2;
      framingRect = new Rect(leftOffset, topOffset, leftOffset + width, topOffset + height);
      if (PRINT_LOG) {
        Log.d(TAG, "Calculated framing rect: " + framingRect);
      }
    }
    return framingRect;
  }
  
  private static int findDesiredDimensionInRange(int resolution, int hardMin, int hardMax) {
    int dim = 5 * resolution / 8; // Target 5/8 of each dimension
    if (dim < hardMin) {
      return hardMin;
    }
    if (dim > hardMax) {
      return hardMax;
    }
    return dim;
  }

  /**
   * Like {@link #getFramingRect} but coordinates are in terms of the preview frame,
   * not UI / screen.
   *
   * @return {@link Rect} expressing barcode scan area in terms of the preview size
   */
  @SuppressWarnings("SuspiciousNameCombination")
  public synchronized Rect getFramingRectInPreview() {
    if (framingRectInPreview == null) {
      Rect framingRect = getFramingRect();
      if (framingRect == null) {
        return null;
      }
      Rect rect = new Rect(framingRect);
      Point cameraTempResolution = configManager.getCameraResolution();
      // Point screenResolution = configManager.getScreenResolution();
      if (cameraTempResolution == null || configManager.getScreenResolution() == null) {
        // Called early, before init even finished
        return null;
      }

      Point cameraResolution;
      if (configManager.getCWNeededRotation() == 90 ||
              configManager.getCWNeededRotation() == 270) {
        cameraResolution = new Point(cameraTempResolution.y, cameraTempResolution.x);
      } else {
        cameraResolution = cameraTempResolution;
      }

      Point screenResolution = new Point(viewSize.width(), viewSize.height());

      rect.left   = rect.left   - viewSize.left;
      rect.right  = rect.right  - viewSize.left;
      rect.top    = rect.top    - viewSize.top;
      rect.bottom = rect.bottom - viewSize.top;

      if (Configs.PRINT_LOG) {
        Log.e(Configs.TAG, "rect 1 = " + rect);
      }

      float ratioX = cameraResolution.x / (float) screenResolution.x;
      float ratioY = cameraResolution.y / (float) screenResolution.y;

      rect.left   = (int) (rect.left   * ratioX);
      rect.right  = (int) (rect.right  * ratioX);
      rect.top    = (int) (rect.top    * ratioY);
      rect.bottom = (int) (rect.bottom * ratioY);

      if (Configs.PRINT_LOG) {
        Log.e(Configs.TAG, "rect 2 = " + rect);
      }

      if (configManager.getCWNeededRotation() == 90 ||
              configManager.getCWNeededRotation() == 270) {
        framingRectInPreview = new Rect(rect.top,
                rect.left, rect.bottom, rect.right);
      } else {
        framingRectInPreview = rect;
      }
    }
    return framingRectInPreview;
  }

  
  /**
   * Allows third party apps to specify the camera ID, rather than determine
   * it automatically based on available cameras and their orientation.
   *
   * @param cameraId camera ID of the camera to use. A negative value means "no preference".
   */
  public synchronized void setManualCameraId(int cameraId) {
    requestedCameraId = cameraId;
  }
  
  /**
   * Allows third party apps to specify the scanning rectangle dimensions, rather than determine
   * them automatically based on screen resolution.
   *
   * @param width The width in pixels to scan.
   * @param height The height in pixels to scan.
   */
  public synchronized void setManualFramingRect(int width, int height) {
    if (initialized) {
      //if (framingRect == null || framingRect.width() != width || framingRect.height() != height) {
        Point screenResolution = configManager.getScreenResolution();
        if (width > screenResolution.x) {
          width = screenResolution.x;
        }
        if (height > screenResolution.y) {
          height = screenResolution.y;
        }
        int leftOffset = (screenResolution.x - width) / 2;
        int topOffset = (screenResolution.y - height) / 2;
        framingRect = new Rect(leftOffset, topOffset, leftOffset + width, topOffset + height);
        if (PRINT_LOG) {
          Log.d(TAG, "Calculated manual framing rect: " + framingRect);
        }
        framingRectInPreview = null;
      //}
    } else {
      requestedFramingRectWidth = width;
      requestedFramingRectHeight = height;
    }
  }

  private Rect tempRect;

  public synchronized void setManualFramingRect(@NonNull Rect rect) {
    if (initialized) {
      if (framingRect == null || !rect.equals(framingRect)) {
        framingRect = rect;
        if (PRINT_LOG) {
          Log.d(TAG, "Calculated manual framing rect: " + framingRect);
        }
        framingRectInPreview = null;
      }
    } else {
      tempRect = rect;
    }
  }

  /**
   * A factory method to build the appropriate LuminanceSource object based on the format
   * of the preview buffers, as described by Camera.Parameters.
   *
   * @param data A preview frame.
   * @param width The width of the image.
   * @param height The height of the image.
   * @return A PlanarYUVLuminanceSource instance.
   */
  @Nullable
  public PlanarYUVLuminanceSource buildLuminanceSource(byte[] data, int width, int height) {
    Rect rect = getFramingRectInPreview();
    if (rect == null) {
      return null;
    }
    // Go ahead and assume it's YUV rather than die.
    return new PlanarYUVLuminanceSource(data, width, height, rect.left, rect.top,
                                        rect.width(), rect.height(), false);
  }

}
