package com.google.camera

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.hardware.camera2.*
import android.os.*
import androidx.fragment.app.Fragment
import android.view.*
import java.lang.ref.WeakReference

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
class CameraFragment: Fragment() {

    private val surfaceView: SurfaceView? get() {
        return view as? SurfaceView
    }
    private val cameraManager by lazy {
        activity?.getSystemService(Context.CAMERA_SERVICE) as? CameraManager
    }
    private val cameraId: String? get() {
        cameraManager?.cameraIdList?.let { cameraIds ->
            for (id in cameraIds) {
                val cameraCharacteristics = cameraManager?.getCameraCharacteristics(id)

                if (CameraMetadata.LENS_FACING_BACK ==
                        cameraCharacteristics?.get(CameraCharacteristics.LENS_FACING)) {
                    return id
                }
            }

            if (cameraIds.isNotEmpty()) {
                return cameraIds[0]
            }
        }

        return null
    }
    private val handler by lazy {
        Handler()
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        return SurfaceView(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        surfaceView?.holder?.addCallback(HolderCallback(this))
    }

    @SuppressLint("MissingPermission")
    private fun openCamera(holder: SurfaceHolder, width: Int, height: Int) {
        cameraId?.let {
            val list = arrayListOf(holder.surface)
            cameraManager?.openCamera(it,
                    StateCallback(this, list),
                    handler)
        }
    }

    private class StateCallback(
            fm: CameraFragment,
            private val surfaces: List<Surface>
    ): CameraDevice.StateCallback() {

        private val fragmentReference = WeakReference(fm)

        override fun onOpened(camera: CameraDevice?) {
            fragmentReference.get()?.let { fm ->
                camera?.createCaptureSession(
                        surfaces, CameraSurfaceWatcher(fm), fm.handler)
            }
        }

        override fun onDisconnected(camera: CameraDevice?) {

        }

        override fun onError(camera: CameraDevice?, error: Int) {

        }
    }

    private class CameraSurfaceWatcher(
            fm: CameraFragment
    ): CameraCaptureSession.StateCallback() {

        private val fragmentReference = WeakReference(fm)

        override fun onConfigureFailed(session: CameraCaptureSession?) {

        }

        override fun onConfigured(session: CameraCaptureSession?) {

        }
    }

    private class HolderCallback(fm: CameraFragment): SurfaceHolder.Callback2 {

        private val fragmentReference = WeakReference(fm)

        override fun surfaceRedrawNeeded(holder: SurfaceHolder?) {}

        override fun surfaceChanged(holder: SurfaceHolder?, format: Int,
                                    width: Int, height: Int) {
            holder?.let {
                fragmentReference.get()?.openCamera(it, width, height)
            }
        }

        override fun surfaceDestroyed(holder: SurfaceHolder?) {}

        override fun surfaceCreated(holder: SurfaceHolder?) {}
    }
}